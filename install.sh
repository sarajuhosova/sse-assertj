# install necessary measuring tools
brew install brightness
brew install blueutil

# clone and compile repos
git clone git@github.com:sarajuhosova/guava-base-junit.git
cd guava-base-junit
mvn clean
mvn compile
cd ..

git clone git@github.com:sarajuhosova/guava-base-assertj.git
cd guava-base-assertj
mvn clean
mvn compile
cd ..
