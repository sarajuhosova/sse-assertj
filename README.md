# AssertJ: How Green is your Test Suite?

This repository includes four scripts which were used to evaluate the power usage of [AssertJ](https://github.com/assertj/assertj-core) in comparison with [JUnit](https://github.com/junit-team/junit5). The result of the study can be found [here](https://emoji.gg/assets/emoji/4081_SoonTM.png).

### `macos-run.sh`

The first script runs selected tests from the `base` package of [Guava Tests](https://github.com/google/guava/tree/master/guava-tests) from two repositories, one that uses [JUnit](https://github.com/sarajuhosova/guava-base-junit) assertions and one that uses [AssertJ](https://github.com/sarajuhosova/guava-base-assertj) assertions. This script is called `macos-run.sh` and it uses Maven to clean and test the repositories. It also turns off Bluetooth and WiFi and sets the screen brightness to 50%.

### `loop.sh`

If you want to run the script several times with some sleep in between, it is possible to use the `loop.sh` script, with following options:

``./loop.sh [-r number_of_runs] [-s sleepy_time_in_seconds] [-o output_folder] [-i installation_script] macos-run``

The default values for the first three options are `-r 30`, `-s 60`, and `-o output`. The installation script does not have a default.

### `install.sh`

To install necessary utils (for adjusting brightness and turning off bluetooth) and to clone and compile the two repositories, the `loop.sh` script should be run at least with the option `-i install`.

### `data_analysis.py`

The two methods used to (1) extract relevant data from Intel Power Gadget output and (2) create violin plots can be found in `data_analysis.py`. 
The data we obtained can be found in a zip file in folder 'results', along with the generated plots.

### Setup

Before running the experiment, don't forget to also:

- have Intel Power Gadget installed ([installation instructions](https://www.intel.com/content/www/us/en/developer/articles/tool/power-gadget.html)),
- run a warm-up to ensure that the hardware is not cold and therefore doesn’t skew the results to a lower energy consumption in the first runs,
- charge the laptop to 100% and keep it plugged in throughout the experiment to ensure equal battery capacity for all runs,
- quit all active applications,
- turn off AirDrop,
- set the screen saver to only appear after one hour,
- turn off the automatic adjustment of brightness,
- turn off keyboard lighting.
