import os
import pandas
import plotly.graph_objects as go


def violin_plot(property, data="results/processed_data.csv", save_html=False, save_png=False):
    """
    Creates a violin plot comparing the tests with two different kinds of assertions.

    :param property: (str) Property to plot, either 'time' or 'power'.
    :param data: (str) Filename with processed data.
    :param save_html:
    :param save_png:
    :return:
    """
    df = pandas.read_csv(data)

    fig = go.Figure()

    if property == "time":
        prop = ['assertj_time', 'junit_time']
        p_name = ['AssertJ', 'JUnit', 'Total Elapsed Time (s)']
    elif property == "power":
        prop = ['assertj_power', 'junit_power']
        p_name = ['AssertJ', 'JUnit', 'Average Power (W)']
    elif property == "energy":
        prop = ['assertj_energy', 'junit_energy']
        p_name = ['AssertJ', 'JUnit', 'Energy Consumed (J)']

    for i, p in enumerate(prop):
        fig.add_trace(go.Violin(y=df[p],
                                name=p_name[i],
                                box_visible=True,
                                meanline_visible=True))

    fig.update_layout(xaxis_title="Assertion Library", yaxis_title=p_name[2])
    fig.show()

    if save_html:
        fig.write_html("results/" + property + "_plots.html")
    if save_png:
        fig.write_image("results/" + property + "_plots.png")  # need to have kaleido installed for this


def extract_data(folder="output", save_filename="results/processed_data.csv"):
    """
    Gets relevant data (total elapsed time and average power) from all files containing
    'junit' or 'assertj' in the specified folder.
    :param folder: (str) Path to folder containing raw data.
    :param save_filename: (str) Path to file in which the processed data is saved.
    :return:
    """
    assertj_time = []
    assertj_power = []
    assertj_energy = []
    junit_time = []
    junit_power = []
    junit_energy = []

    for filename in os.listdir(folder):
        with open(folder + "/" + filename, "r") as file:
            if "assertj" in filename:
                lines = file.readlines()

                # retrieve float values
                elapsed_time = float(lines[-14].split(" = ")[1][:-2])
                average_power = float(lines[-9].split(" = ")[1][:-2])
                energy_consumed = float(lines[-11].split(" = ")[1][:-2])

                assertj_time.append(elapsed_time)
                assertj_power.append(average_power)
                assertj_energy.append(energy_consumed)

            elif "junit" in filename:
                lines = file.readlines()

                # retrieve float values
                elapsed_time = float(lines[-14].split(" = ")[1][:-2])
                average_power = float(lines[-9].split(" = ")[1][:-2])
                energy_consumed = float(lines[-11].split(" = ")[1][:-2])

                junit_time.append(elapsed_time)
                junit_power.append(average_power)
                junit_energy.append(energy_consumed)

    data = {
        "junit_time": junit_time,
        "junit_power": junit_power,
        "junit_energy": junit_energy,
        "assertj_time": assertj_time,
        "assertj_power": assertj_power,
        "assertj_energy": assertj_energy
    }
    df = pandas.DataFrame(data, columns=["junit_time", "junit_power", "junit_energy",
                                         "assertj_time", "assertj_power", "assertj_energy"])
    df.to_csv(save_filename)


if __name__ == '__main__':
    # extract_data()
    violin_plot(property="time")
    violin_plot(property="power")
    violin_plot(property="energy")
