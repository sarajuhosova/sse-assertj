# freeze settings
brightness 0.5
blueutil -p 0
networksetup -setairportpower en0 off

DT=`date | tr -d ' :'`

# clone junit tests

cd guava-base-junit
mvn clean
/Applications/Intel\ Power\ Gadget/PowerLog -file ../output/junit-macos-o-$DT.csv -cmd mvn test
cd ..

# clone assertj

cd guava-base-assertj
mvn clean
/Applications/Intel\ Power\ Gadget/PowerLog -file ../output/assertj-macos-o-$DT.csv -cmd mvn test
networksetup -setairportpower en0 on
cd ..
