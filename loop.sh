#!/bin/bash

loading_bar(){
	STRING="|"
	for (( j=0; j<$2;         j++ )); do STRING+="#"; done
	for (( k=0; k<$(($1-$2)); k++ )); do STRING+="-"; done
	STRING+="| ${2}/${1}"
}

RUN=30
SLP=60
FLDR=output

while getopts 'r:s:o:i:' opt; do
    case $opt in
      r)   RUN=$OPTARG;;
      s)   SLP=$OPTARG;;
      o)   FLDR=$OPTARG;;
			i)
					 INSTL_SCRIPT=$OPTARG
					 echo "Installing requirements from ${INSTL_SCRIPT}"
					 chmod +x $INSTL_SCRIPT.sh
					 ./$INSTL_SCRIPT.sh
					 ;;
    esac
done

if [ $(( $# - $OPTIND )) -lt 0 ]; then
    echo "Usage: `./loop.sh` [options] script"
    exit 1
fi

SCRPT=${@:$OPTIND:1}

echo "Running ${SCRPT}.sh ${RUN} times. Sleeping ${SLP} second between runs."

if [ ! -d $FLDR ]; then
  mkdir $FLDR;
fi

chmod +x $SCRPT.sh

for (( i=0; i<RUN; i++ ))
do
	./$SCRPT.sh $FLDR $i

	loading_bar $RUN $(($i+1))

	if [ $i -lt $(($RUN-1)) ]; then
		echo -ne "$STRING\r"
		sleep $SLP
	else
		echo "$STRING"
	fi
done

echo "Results have been output to the ${FLDR} folder."
